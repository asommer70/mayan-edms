# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Thomas Lauterbach <lauterbachthomas@googlemail.com>, 2023
# Bjoern Kowarsch <bjoern.kowarsch@gmail.com>, 2023
# Marvin Haschker <marvin@haschker.me>, 2023
# Mathias Behrle <mathiasb@m9s.biz>, 2023
# Dennis M. Pöpperl <free-software@dm-poepperl.de>, 2023
# Roberto Rosario, 2023
# Berny <berny@bernhard-marx.de>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:55+0000\n"
"Last-Translator: Berny <berny@bernhard-marx.de>, 2023\n"
"Language-Team: German (Germany) (https://www.transifex.com/rosarior/teams/13584/de_DE/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de_DE\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:27 permissions.py:6 settings.py:15
msgid "Storage"
msgstr "Dateispeicher"

#: classes.py:88 classes.py:109
#, python-format
msgid ""
"Unable to initialize storage: %(name)s. Check the storage backend dotted "
"path and arguments."
msgstr ""
"Dateispeicher kann nicht initialisiert werden: %(name)s. Überprüfen Sie den "
"Pfad und die Argumente für das Speicher-Backend."

#: events.py:6 links.py:20 models.py:56 storages.py:22
msgid "Download files"
msgstr "Dateien herunterladen"

#: events.py:10
msgid "Download file created"
msgstr ""

#: events.py:13
msgid "Download file deleted"
msgstr ""

#: events.py:16
msgid "Download file downloaded"
msgstr ""

#: links.py:12
msgid "Delete"
msgstr "Löschen"

#: links.py:16
msgid "Download"
msgstr "Herunterladen"

#: management/commands/storage_process.py:13
msgid "Name of the app to process."
msgstr "Name der zu verarbeitenden App."

#: management/commands/storage_process.py:19
msgid ""
"Path of the database (.dbm) file that will be created/read to keep track of "
"items processed."
msgstr ""
"Pfad der Datenbankdatei (.dbm), die erstellt / gelesen wird, um die "
"verarbeiteten Elemente zu verfolgen."

#: management/commands/storage_process.py:26
msgid "Process a specific model."
msgstr "Ein bestimmtes Modell ausführen."

#: management/commands/storage_process.py:32
msgid ""
"Process the files in reverse to undo the storage pipeline transformations."
msgstr ""
"Die Dateien in umgekehrter Reihenfolge um die Dateispeicher-Pipeline-"
"Transformation rückgängig zu machen."

#: management/commands/storage_process.py:38
msgid "Name of the storage to process."
msgstr "Name des zu verarbeitenden Dateispeichers."

#: model_mixins.py:18 models.py:94
msgid "Filename"
msgstr "Dateiname"

#: model_mixins.py:21
msgid "Date time"
msgstr "Datum & Zeit"

#: model_mixins.py:75
msgid "Unnamed file"
msgstr "Unbenannte Datei"

#: model_mixins.py:94
msgid "Size"
msgstr "Größe"

#: model_mixins.py:101 models.py:48
msgid "User"
msgstr "Benutzer"

#: models.py:40 models.py:91
msgid "File"
msgstr "Datei"

#: models.py:43
msgid "Label"
msgstr "Bezeichner"

#: models.py:55
msgid "Download file"
msgstr "Datei herunterladen"

#: models.py:100
msgid "Shared uploaded file"
msgstr "Geteilte hochgeladene Datei"

#: models.py:101 storages.py:36
msgid "Shared uploaded files"
msgstr "Geteilte hochgeladene Dateien"

#: permissions.py:10
msgid "Delete user files"
msgstr ""

#: permissions.py:13
msgid "Download user files"
msgstr ""

#: permissions.py:16
msgid "View user files"
msgstr ""

#: queues.py:13
msgid "Storage periodic"
msgstr ""

#: queues.py:19
msgid "Delete stale uploads"
msgstr "Veraltete Uploads löschen"

#: queues.py:26
msgid "Delete stale download files"
msgstr "Veraltete Downloaddateien löschen"

#: settings.py:21
msgid "Time in seconds, after which download files will be deleted."
msgstr ""

#: settings.py:27
msgid ""
"A storage backend that all workers can use to generate and hold files for "
"download."
msgstr ""

#: settings.py:38
msgid "A storage backend that all workers can use to share files."
msgstr "Datenbackend, das alle Worker benutzen können, um Dateien zu teilen."

#: settings.py:48
msgid ""
"Temporary directory used site wide to store thumbnails, previews and "
"temporary files."
msgstr ""
"Temporäres Verzeichnis zum systemweiten Speichern von Miniaturbildern, "
"Vorschauen und temporären Dateien. "

#: settings.py:55
msgid "Time in seconds, after which temporary uploaded files will be deleted."
msgstr ""

#: storages.py:15
msgid ""
"Unable to initialize the download file storage. Check the settings {} and {}"
" for formatting errors."
msgstr ""
"Initialisierung des Dateispeichers für Downloads nicht möglich. Überprüfen "
"Sie die Einstellungen {} und {} auf Formatierungsfehler."

#: storages.py:29
msgid ""
"Unable to initialize the shared uploaded file storage. Check the settings {}"
" and {} for formatting errors."
msgstr ""
"Der gemeinsam genutzte Dateispeicher für hochgeladene Dateien kann nicht "
"initialisiert werden. Überprüfen Sie die Einstellungen {} und {} auf "
"Formatierungsfehler."

#: views.py:67
msgid ""
"Download files are created as a results of a an external process like an "
"export. Download files are retained over a span of time and then removed "
"automatically."
msgstr ""

#: views.py:71
msgid "There are no files to download."
msgstr "Keine Dateien für Download vorhanden."

#: views.py:72
msgid "Downloads"
msgstr "Downloads"
