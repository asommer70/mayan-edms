# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Jože Detečnik <joze.detecnik@3tav.si>, 2023
# Roberto Rosario, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:49+0000\n"
"Last-Translator: Roberto Rosario, 2023\n"
"Language-Team: Slovenian (https://www.transifex.com/rosarior/teams/13584/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: apps.py:26 classes.py:200 links.py:27
msgid "ACLs"
msgstr "Pravice"

#: apps.py:62
msgid ""
"Object for which the access is granted. When sorting objects, only the type "
"is used and not the actual label of the object."
msgstr ""

#: apps.py:65
msgid "Object"
msgstr "Predmet"

#: events.py:6 permissions.py:6
msgid "Access control lists"
msgstr "Seznami za nadzor dostopa"

#: events.py:10
msgid "ACL created"
msgstr ""

#: events.py:13
msgid "ACL deleted"
msgstr ""

#: events.py:16
msgid "ACL edited"
msgstr ""

#: forms.py:13 models.py:56 serializers.py:79 views.py:73
msgid "Role"
msgstr "Vloga"

#: links.py:16
msgid "New ACL"
msgstr "Novi ACL"

#: links.py:21
msgid "Delete"
msgstr "Izbriši"

#: links.py:32 models.py:50 workflow_actions.py:46 workflow_actions.py:166
msgid "Permissions"
msgstr "Pravice"

#: links.py:35
msgid "Global ACLs"
msgstr ""

#: managers.py:267
#, python-format
msgid "Object \"%s\" is not a model and cannot be checked for access."
msgstr ""

#: managers.py:287
#, python-format
msgid "Insufficient access for: %s"
msgstr "Nezadosten dostop za: %s"

#: model_mixins.py:23
msgid "Permission count"
msgstr ""

#: models.py:40 serializers.py:16
msgid "Content type"
msgstr ""

#: models.py:43 workflow_actions.py:32
msgid "Object ID"
msgstr "ID objekta"

#: models.py:54
msgid "Role to which the access is granted for the specified object."
msgstr ""

#: models.py:64
msgid "Access entry"
msgstr "Vstopna točka"

#: models.py:65
msgid "Access entries"
msgstr "Vstopne točke"

#: models.py:69
#, python-format
msgid "Role \"%(role)s\" permission's for \"%(object)s\""
msgstr "Vloga »%(role)s«  dovoljenja za »%(object)s«"

#: permissions.py:10
msgid "Edit ACLs"
msgstr "Uredi dostopne pravice"

#: permissions.py:13
msgid "View ACLs"
msgstr "Preglej dostopne pravice"

#: serializers.py:19
msgid "Permissions add URL"
msgstr ""

#: serializers.py:39
msgid "Permissions remove URL"
msgstr ""

#: serializers.py:59
msgid "Permissions URL"
msgstr ""

#: serializers.py:82
msgid "Role ID"
msgstr ""

#: serializers.py:85
msgid "URL"
msgstr ""

#: serializers.py:118
msgid "Primary key of the permission to add to the ACL."
msgstr ""

#: serializers.py:119 serializers.py:126
msgid "Permission ID"
msgstr ""

#: serializers.py:125
msgid "Primary key of the permission to remove from the ACL."
msgstr ""

#: views.py:44
#, python-format
msgid ""
"An ACL for \"%(object)s\" using role \"%(role)s\" already exists. Edit that "
"ACL entry instead."
msgstr ""

#: views.py:59
#, python-format
msgid "New access control lists for: %s"
msgstr "Novi seznami za nadzor dostopa za: %s"

#: views.py:103
#, python-format
msgid "Delete ACL: %s"
msgstr "Brisanje ACL: %s"

#: views.py:149
msgid "There are no ACLs for this object"
msgstr ""

#: views.py:152 views.py:272
msgid ""
"ACL stands for Access Control List and is a precise method to control user "
"access to objects in the system. ACLs allow granting a permission to a role "
"but only for a specific object or set of objects."
msgstr ""

#: views.py:159
#, python-format
msgid "Access control lists for: %s"
msgstr "Dostopne pravice za %s"

#: views.py:168
msgid "Granted permissions"
msgstr "Dodeljena dovoljenja"

#: views.py:169
msgid "Available permissions"
msgstr "Razpoložljiva dovoljenja"

#: views.py:219
#, python-format
msgid "Role \"%(role)s\" permission's for \"%(object)s\"."
msgstr ""

#: views.py:228
msgid ""
"Disabled permissions are inherited from a parent object or directly granted "
"to the role and can't be removed from this view. Inherited permissions need "
"to be removed from the parent object's ACL or from them role via the Setup "
"menu."
msgstr ""
"Onemogočena dovoljenja so podedovana od nadrejenega predmeta ali neposredno "
"dodeljena vlogi in je ni mogoče odstraniti iz tega pogleda. Podedovana "
"dovoljenja je treba odstraniti iz nadrejenega predmeta ali vloge v "
"nastavitvah."

#: views.py:269
msgid "There are no ACLs"
msgstr ""

#: views.py:278
msgid "Global access control lists"
msgstr ""

#: workflow_actions.py:23
msgid "Object type"
msgstr "Vrsta objekta"

#: workflow_actions.py:26
msgid "Type of the object for which the access will be modified."
msgstr "Vrsta predmeta, za katerega bo dostop spremenjen."

#: workflow_actions.py:35
msgid ""
"Numeric identifier of the object for which the access will be modified."
msgstr "Številčni identifikator predmeta, za katerega bo dostop spremenjen."

#: workflow_actions.py:40 workflow_actions.py:160
msgid "Roles"
msgstr "Vloge"

#: workflow_actions.py:42 workflow_actions.py:162
msgid "Roles whose access will be modified."
msgstr "Vloge, katerih dostop bo spremenjen."

#: workflow_actions.py:49 workflow_actions.py:169
msgid ""
"Permissions to grant/revoke to/from the role for the object selected above."
msgstr "Dovoljenja za odobritev/preklic v/iz vlog za zgoraj izbrani objekt."

#: workflow_actions.py:57
msgid "Grant object access"
msgstr ""

#: workflow_actions.py:145
msgid "Revoke object access"
msgstr ""

#: workflow_actions.py:177
msgid "Grant document access"
msgstr ""

#: workflow_actions.py:218
msgid "Revoke document access"
msgstr ""
